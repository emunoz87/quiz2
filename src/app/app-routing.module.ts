import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'main',
    loadChildren: () => import('./main/main.module').then( m => m.MainPageModule)
  },
  {
    path: '',
    redirectTo: 'main',
    pathMatch: 'full'
  },
  {
    path: 'main',
    children:[
      {
        path: '',
        loadChildren: "./main/main.module#MainPageModule",
      },
      {
        path: 'add',
        loadChildren: "./main/add/add.module#AddPageModule",
      },
      {
        path: 'add-event',
        loadChildren: "./main/add-event/add-event.module#AddEventPageModule",
      },
      {
        path: 'view/:matchId',
        loadChildren: "./main/view/view.module#ViewPageModule",
      },
      {
        path: 'view/:eventId',
        loadChildren: "./main/view/view.module#ViewPageModule",
      },
    ],
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
