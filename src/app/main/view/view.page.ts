import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { MainService } from '../main.service';
import { match, event } from '../main.model';

@Component({
  selector: 'app-view',
  templateUrl: './view.page.html',
  styleUrls: ['./view.page.scss'],
})
export class ViewPage implements OnInit {
  @Input() teamOne: string; 
  @Input() teamTwo: string;
  @Input() eventName: string;

  event: event;
  match: match;
  

  constructor(
    private activeRouter: ActivatedRoute,
    private MainService: MainService,
    private router: Router,
    private alertController: AlertController
  ) { }

  ngOnInit() {

    this.activeRouter.paramMap.subscribe(
      paramMap => {
        if(!paramMap.has('matchId')){
          return;
        }
        const matchId =  parseInt( paramMap.get('matchId'));
        this.match = this.MainService.getMatch(matchId);
        console.log(matchId);
      }
    );  

    this.activeRouter.paramMap.subscribe(
      paramMap => {
        if(!paramMap.has('eventId')){
          return;
        }
        const eventId =  parseInt( paramMap.get('eventId'));
        this.event = this.MainService.getEvent(eventId);
        console.log(eventId);
      }
    );
  }

  viewEvent(codeEvent: number) {
    this.router.navigate(["/main/view/" + codeEvent]);
  }
}


