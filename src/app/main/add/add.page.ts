import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { MainService } from "../main.service";
import { match,event } from "../main.model";

@Component({
  selector: 'app-add',
  templateUrl: './add.page.html',
  styleUrls: ['./add.page.scss'],
})
export class AddPage implements OnInit {
  formMatchAdd: FormGroup;
  
  constructor(private serviceMain: MainService, private router: Router) { }

  ngOnInit() {
    this.formMatchAdd = new FormGroup({
      pcodeMatch: new FormControl(1, {
        updateOn: "blur",
        validators: [Validators.required, Validators.min(1)],
      }),
      pteamOne: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required, Validators.minLength(1)],
      }),
      pteamTwo: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required, Validators.minLength(1)],
      }),
      pscoreOne: new FormControl(0, {
        updateOn: "blur",
        validators: [Validators.required, Validators.minLength(1)],
      }),
      pscoreTwo: new FormControl(0, {
        updateOn: "blur",
        validators: [Validators.required, Validators.minLength(1)],
      }),
      pevent: new FormControl(0, {
        updateOn: "blur",
        validators: [Validators.required, Validators.minLength(1)],
      }),
    });
  }

  addMatch() {

    if (!this.formMatchAdd.valid) {
      return;
    }

    this.serviceMain.addMatch(
      this.formMatchAdd.value.pcodeMatch,
      this.formMatchAdd.value.pteamOne,
      this.formMatchAdd.value.pteamTwo,
      this.formMatchAdd.value.pscoreOne,
      this.formMatchAdd.value.pscoreTwo,
    );

    this.formMatchAdd.reset();
    
    this.router.navigate(["/main"]);
    
  }

}
