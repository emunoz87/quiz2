import { Injectable } from '@angular/core';
import { match,event } from "./main.model";


@Injectable({
  providedIn: 'root'
})
export class MainService {

  private event: event[] = [];
  private match: match[] = [];

  constructor() { }

  getAll() {
    return [...this.match];
  }

  getMatch(matchId: number) {
    return {
      ...this.match.find((match) => {
        return match.codeMatch === matchId;
      }),
    };
  }

  getEvent(eventId: number) {
     return {
       ...this.event.find((event) => {
         return event.codeEvent === eventId;
       }),
     };
   }

  
  addMatch(
    pcodeMatch: number,
    pteamOne: string,
    pteamTwo: string,
    pscoreOne: number,
    pscoreTwo: number,  
  ) {
    const match: match = {
      codeMatch: pcodeMatch,
      teamOne: pteamOne,
      teamTwo: pteamTwo,
      scoreOne: pscoreOne,
      scoreTwo: pscoreTwo,
      event: this.event,
    }
    this.match.push(match);
  }

   addEvent(
     pcodeEvent: number,
     peventName: string,
   ) {
     const event: event = {
       codeEvent: pcodeEvent,
       eventName: peventName,
     }
     this.event.push(event);
  }
}
