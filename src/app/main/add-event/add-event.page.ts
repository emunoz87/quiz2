import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { MainService } from "../main.service";

@Component({
  selector: 'app-add-event',
  templateUrl: './add-event.page.html',
  styleUrls: ['./add-event.page.scss'],
})
export class AddEventPage implements OnInit {
  formEventAdd: FormGroup;

  constructor(private serviceMain: MainService, private router: Router) { }

  ngOnInit() {
    this.formEventAdd = new FormGroup({
      pcodeEvent: new FormControl(1, {
        updateOn: "blur",
        validators: [Validators.required, Validators.min(1)],
      }),
      peventName: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required, Validators.minLength(1)],
      }),
    });
  }

  addEvent() {

    if (!this.formEventAdd.valid) {
      return;
    }

    this.serviceMain.addEvent(
      this.formEventAdd.value.pcodeEvent,
      this.formEventAdd.value.pnameEvent,
    );

    this.formEventAdd.reset();
    
    this.router.navigate(["/main"]);
    
  }

}
