export interface match{
    codeMatch: number;
    teamOne: string;
    teamTwo: string;
    scoreOne: number;
    scoreTwo: number;
    event: event[];
}

export interface event{
    codeEvent: number;
    eventName: string;
}