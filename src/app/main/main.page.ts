import { Component, Input, OnInit } from '@angular/core';
import { MainService } from "./main.service";
import { match, event } from "./main.model";
import { Router } from "@angular/router";
import { AlertController } from "@ionic/angular";

@Component({
  selector: 'app-main',
  templateUrl: './main.page.html',
  styleUrls: ['./main.page.scss'],
})
export class MainPage implements OnInit {
  @Input() teamOne: string; 
  @Input() teamTwo: string;
  @Input() eventName: string;
  

  match: match[];
  event: event[];
  
  constructor(
    private MainServices: MainService,
    private router: Router,
    private alertController: AlertController
  ) { }

  ngOnInit() {
    console.log("Carga inicial");
    this.match = this.MainServices.getAll();
  }

  ionViewWillEnter() {
    console.log(this.match);
    this.match = this.MainServices.getAll();
  }

  view(codeMatch: number) {
    this.router.navigate(["/main/view/" + codeMatch]);
  }

  viewEvent(codeEvent: number) {
    this.router.navigate(["/main/view/" + codeEvent]);
  }

}
